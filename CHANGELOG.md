# Change Log

All notable changes to the "crestron-simpl-plus" extension will be documented in this file.

## 1.4.4
 - Added support for signed_integer_function keyword
 - Add support for .usl file type

## 1.4.3
 - Added support for byref, byval, readonlybyref keywords
 - Updated keybind for compile for 3 & 4 series

## 1.4.2
 - Added support for continue keyword

## 1.4.1
 - Added support for CMutex syntax and functions
 - Added support for CEvent syntax and functions

## 1.4.0

- Added support for compiling for 4 series
- Added support for compiling for 3 and 4 series

## 1.3.0

- Added support for opening and generating Simpl# API files.
- Syntax highlighting for Simpl# API files.
- Fixed an issue in the visualizer with multiple signal detection.

## 1.2.3

- Minor additional syntax highlighting improvements.

## 1.2.2

- Big improvements to syntax detection. (Function highlighting, signal name highlighting, etc)
- Various Syntax related bug fixes (special characters breaking string detection, improper highlighting, etc)


## 1.2.1

- Add 'threadsafe' to syntax detection.
- Handle '#output_shift' compiler directive in visualizer.

## 1.2.0

- Added a live visualizer to preview your modules in real time.

## 1.1.0

- Remove code snippets from main plugin and place them in a separate extension.

## 1.0.0

- Initial release
